db.fruits.aggregate([
	{
		$match : {onSale : true}
	},
	{
		$count : "fruits_onSale"
	}
])

db.fruits.aggregate([
	{
		$match : {stock : {$gt : 20}}
	},
	{
		$count : "fruits_with_more_than_20_stock"
	}
])

db.fruits.aggregate([
	{
		$match : {onSale : true}
	},
	{
		$group : {_id : "$supplier_id", averagePrice : {$avg : "$price"}}
	}
])

db.fruits.aggregate([
	{
		$group : {_id : "$supplier_id", highestPrice : {$max : "$price"}}
	}
])

db.fruits.aggregate([
	{
		$group : {_id : "$supplier_id", lowestPrice : {$min : "$price"}}
	}
])